const Observable = require("tns-core-modules/data/observable").Observable;
const fromObject = require("tns-core-modules/data/observable").fromObject;
const ObservableArray = require("tns-core-modules/data/observable-array").ObservableArray;
var pageModule = require("ui/page");

function createViewModel(db) {
    const viewModel = new Observable();

    const entryFromDB = row => {
        /* just to map entry on the db with in memory state here
         * binding functions such as onDelete
         */
        const onDelete = (event) => {
            let entry = event.object._observers.tap[0].thisArg;
            viewModel.set('entries', new ObservableArray(
                viewModel.entries.filter(e => e.entry_id != entry.entry_id)
            ));
            db.all(`DELETE FROM entries where id = ${entry.entry_id}`).then(rows => {
                console.log(`DELETED ${rows}`);
            }, error => {
                console.log(`ERROR DELETING ${error}`);
            });
        };

        const onDone = (event) => {
            let entry = event.object._observers.tap[0].thisArg;
	        db.execSQL(
                'UPDATE entries SET check_date = ? WHERE id = ?',
                [new Date().toISOString(), entry.entry_id]
            ).then(rows => {
                entry.set('done', true);
                console.log('setting done entry:', e);
            }, error => {
                console.error(`ERROR updating ${e}`)
            });
        }

        console.log(row['check_date']);
        console.log('applying', row['id'], ': ', row['check_date'] < new Date().toISOString());
        return fromObject({
            entry_id: row['id'],
            entry_text: row['entry_text'],
            done: row['check_date'] < new Date().toISOString(),
            onDelete: onDelete,
            onDone: onDone
        })
    }


    viewModel.select = function () {
        viewModel.entries = new ObservableArray([]);
        db.all("SELECT * FROM entries").then(rows => {
            for (var row in rows) {
                viewModel.entries.push(entryFromDB(rows[row]));
	        }
	    }, error => {
	        console.log("SELECT ERROR", error);
	    });
    };
    viewModel.select();

    viewModel.onAdd = () => {
        db.execSQL("INSERT INTO entries (entry_text) VALUES (?)", [viewModel.addingText]).then(id => {
            console.log("INSERT RESULT", id);
            viewModel.entries.push(entryFromDB({
                entry_text: viewModel.addingText,
                id: id,
                check_date: null,
            }));
        }, error => {
            console.log("INSERT ERROR", error);
        });
    };

    return viewModel;
}

exports.createViewModel = createViewModel;
