# TodoNS
todo app created with nativescript.

project created with
```bash
tns create TodoNS --js --path /mnt/esterno/axant --appid it.axant.todons
```

for example run this project on android with
```bash
tns run android
```
